# Generated by Django 2.2.1 on 2019-07-31 15:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0007_position_book_value'),
    ]

    operations = [
        migrations.AddField(
            model_name='position',
            name='currencyCode',
            field=models.CharField(max_length=6, null=True),
        ),
        migrations.AddField(
            model_name='position',
            name='securityType',
            field=models.CharField(max_length=15, null=True),
        ),
    ]
