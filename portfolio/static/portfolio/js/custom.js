$(document).ready(function() { 
    var period = $('#financialsPeriod').children("option:selected").val(); 

    // DataTables ----------------------------------------------------------------------------
    // Financials YEARLY AND ASSET ALLOCATION----------------------------------------------------------------------------
    $('#balanceSheetYearlyTable, #incomeStatementYearlyTable, #cashFlowYearlyTable,\
     #assetAllocationTable').DataTable({
        "paging": false,
        "scrollX": true,
        "searching": false,
        "ordering": false,
        "bInfo": false,
        "bAutoWidth": true
    });

    // Financials QUARTERLY ----------------------------------------------------------------------------
    $('#balanceSheetQuarterlyTable, #incomeStatementQuarterlyTable, #cashFlowQuarterlyTable').DataTable({
        "paging": false,
        "scrollX": true,
        "searching": false,
        "ordering": false,
        "bInfo": false,
        "bAutoWidth": true,
        "fixedColumns": true,
        "autoWidth": false
    });
      
 
    // CARDS ---------------------------------------------------------------------------
        // Description Card ----------------------------------------------------------------
        $('#navDescriptionCard').click(function(){
            $('#descriptionCard').show()
            $('#financialsCard').hide()
            $('#assetAllocationWrapper').hide()
            $('a#navDescriptionCard').addClass("active")
            $('a#navFinancialsCard').removeClass("active")
            $('a#navAssetAllocationCard').removeClass("active")
            
        });

        $('#navAssetAllocationCard').click(function(){
            $('#assetAllocationCard').show()
            $('#descriptionCard').hide()
            $('a#navAssetAllocationCard').addClass("active")
            $('a#navDescriptionCard').removeClass("active")

            $('#assetAllocationWrapper').show()
            $('#assetAllocationTable').DataTable().columns.adjust();

        });

        // Financials Card ----------------------------------------------------------------
        $('#navFinancialsCard').click(function(){
            $('#financialsCard').show()
            $('#descriptionCard').hide()
            $('#incomeStatementYearlyWrapper').show()
            $('#incomeStatementYearlyTable').DataTable().columns.adjust();

            $('a#navFinancialsCard').addClass("active")
            $('a#navDescriptionCard').removeClass("active")
        });
              
            // Financial Statement Buttons ----------------------------------------------------------------
            $('#btnBalanceSheet').click(function(){
                
                $( '#btnBalanceSheet' ).removeClass( "btn-outline-info" )
                $( '#btnBalanceSheet' ).addClass( "btn-info" );
                $( '#btnIncomeStatement, #btnCashFlowStatement' ).removeClass("btn-info")
                $( '#btnIncomeStatement, #btnCashFlowStatement' ).addClass("btn-outline-info"); 

                if(period == "yearly"){  
                    $('#balanceSheetYearlyWrapper').show()
                    $('#balanceSheetYearlyTable').DataTable().columns.adjust();

                    $('#incomeStatementYearlyWrapper, #incomeStatementQuarterlyWrapper').hide()
                    $('#cashFlowYearlyWrapper, #cashFlowQuarterlyWrapper').hide()
                    $('#balanceSheetQuarterlyWrapper').hide()
                }
                
                if(period == "quarterly"){
                    $('#balanceSheetQuarterlyWrapper').show()
                    $('#balanceSheetQuarterlyTable').DataTable().columns.adjust();
                     
                    $('#incomeStatementYearlyWrapper, #incomeStatementQuarterlyWrapper').hide()
                    $('#cashFlowYearlyWrapper, #cashFlowQuarterlyWrapper').hide()
                    $('#balanceSheetYearlyWrapper').hide()

                }
            });

            $('#btnIncomeStatement').click(function(){
                
                $( '#btnIncomeStatement' ).removeClass( "btn-outline-info" )
                $( '#btnIncomeStatement' ).addClass( "btn-info" );
                $( '#btnBalanceSheet, #btnCashFlowStatement' ).removeClass( "btn-info" )
                $( '#btnBalanceSheet, #btnCashFlowStatement' ).addClass( "btn-outline-info" );

                if(period == "yearly"){
                    $('#incomeStatementYearlyWrapper').show()
                    $('#incomeStatementYearlyTable').DataTable().columns.adjust();
                    
                    $('#incomeStatementQuarterlyWrapper').hide()     
                    $('#cashFlowYearlyWrapper, #cashFlowQuarterlyWrapper').hide()
                    $('#balanceSheetYearlyWrapper, #balanceSheetQuarterlyWrapper').hide()
                }
                
                if(period == "quarterly"){
                    $('#incomeStatementQuarterlyWrapper').show()
                    $('#incomeStatementQuarterlyTable').DataTable().columns.adjust();
                    
                    $('#incomeStatementYearlyWrapper').hide()
                    $('#cashFlowYearlyWrapper, #cashFlowQuarterlyWrapper').hide()
                    $('#balanceSheetYearlyWrapper, #balanceSheetQuarterlyWrapper').hide()
                }

            });

            $('#btnCashFlowStatement').click(function(){
                $( '#btnCashFlowStatement' ).removeClass( "btn-outline-info" )
                $( '#btnCashFlowStatement' ).addClass( "btn-info" );
                $( '#btnIncomeStatement, #btnBalanceSheet' ).removeClass( "btn-info" )
                $( '#btnIncomeStatement, #btnBalanceSheet' ).addClass( "btn-outline-info" );

                if(period == "yearly"){  
                    $('#cashFlowYearlyWrapper').show()
                    $('#cashFlowYearlyTable').DataTable().columns.adjust();
                    
                    $('#incomeStatementYearlyWrapper, #incomeStatementQuarterlyWrapper').hide()
                    $('#cashFlowQuarterlyWrapper').hide()
                    $('#balanceSheetYearlyWrapper, #balanceSheetQuarterlyWrapper').hide()
                    
                }

                if(period == "quarterly"){
                    $('#cashFlowQuarterlyWrapper').show()
                    $('#cashFlowQuarterlyTable').DataTable().columns.adjust();
                    
                    $('#incomeStatementYearlyWrapper, #incomeStatementQuarterlyWrapper').hide()
                    $('#cashFlowYearlyWrapper').hide()
                    $('#balanceSheetYearlyWrapper, #balanceSheetQuarterlyWrapper').hide()
                }

            });
        
            // Financial Statement Period Change ----------------------------------------------------------------
            $( "#financialsPeriod" ).change(function() { 
                period = $(this).children("option:selected").val();
                
                if(period == "yearly"){
                    // BALANCE SHEET ---------------------------------------------------------
                    if($('#btnBalanceSheet').hasClass( "btn-info" )){
                        $('#balanceSheetYearlyWrapper').show()
                        $('#balanceSheetYearlyTable').DataTable().columns.adjust();

                        $('#incomeStatementYearlyWrapper, #incomeStatementQuarterlyWrapper').hide()
                        $('#cashFlowYearlyWrapper, #cashFlowQuarterlyWrapper').hide()
                        $('#balanceSheetQuarterlyWrapper').hide()
                    }

                    // INCOME STATEMENT ---------------------------------------------------------
                    if($('#btnIncomeStatement').hasClass( "btn-info" )){
                        $('#incomeStatementYearlyWrapper').show()
                        $('#incomeStatementYearlyTable').DataTable().columns.adjust();
                        
                        $('#incomeStatementQuarterlyWrapper').hide()     
                        $('#cashFlowYearlyWrapper, #cashFlowQuarterlyWrapper').hide()
                        $('#balanceSheetYearlyWrapper, #balanceSheetQuarterlyWrapper').hide()
                    }

                    // CASH FLOW STATEMENT ---------------------------------------------------------
                    if($('#btnCashFlowStatement').hasClass( "btn-info" )){
                        $('#cashFlowYearlyWrapper').show()
                        $('#cashFlowYearlyTable').DataTable().columns.adjust();
                        
                        $('#incomeStatementYearlyWrapper, #incomeStatementQuarterlyWrapper').hide()
                        $('#cashFlowQuarterlyWrapper').hide()
                        $('#balanceSheetYearlyWrapper, #balanceSheetQuarterlyWrapper').hide()
                    }
                }

                if(period == "quarterly"){
                    // BALANCE SHEET ---------------------------------------------------------
                    if($( "#btnBalanceSheet" ).hasClass('btn-info')){
                        $('#balanceSheetQuarterlyWrapper').show()
                        $('#balanceSheetQuarterlyTable').DataTable().columns.adjust();
                         
                        $('#incomeStatementYearlyWrapper, #incomeStatementQuarterlyWrapper').hide()
                        $('#cashFlowYearlyWrapper, #cashFlowQuarterlyWrapper').hide()
                        $('#balanceSheetYearlyWrapper').hide()
                    }

                    // INCOME STATEMENT ---------------------------------------------------------
                    if($('#btnIncomeStatement').hasClass('btn-info')){
                        $('#incomeStatementQuarterlyWrapper').show()
                        $('#incomeStatementQuarterlyTable').DataTable().columns.adjust();
                        
                        $('#incomeStatementYearlyWrapper').hide()
                        $('#cashFlowYearlyWrapper, #cashFlowQuarterlyWrapper').hide()
                        $('#balanceSheetYearlyWrapper, #balanceSheetQuarterlyWrapper').hide()
                    }

                    // CASH FLOW STATEMENT ---------------------------------------------------------
                    if($('#btnCashFlowStatement').hasClass('btn-info')){
                        $('#cashFlowQuarterlyWrapper').show()
                        $('#cashFlowQuarterlyTable').DataTable().columns.adjust();
                        
                        $('#incomeStatementYearlyWrapper, #incomeStatementQuarterlyWrapper').hide()
                        $('#cashFlowYearlyWrapper').hide()
                        $('#balanceSheetYearlyWrapper, #balanceSheetQuarterlyWrapper').hide()
                    }
                }
            });
            
    
    

    //- PORTFOLIO DETAILS PAGE- POSITIONS TABLES --------------------------------------------
    var table;

    // POSITION DELETE
    $("#usdPositionsTable, #cadPositionsTable").on("mousedown", "td .fas.fa-trash-alt", function (e) {
        var $row = $(this).closest("tr");
        var symbolId = $row.find("th.col-1 span").text();
        var symbol = $row.find('th.col-1').contents().filter(function () {
            return this.nodeType == 3;
        }).text();
        var quantity = $row.children("td:first").text().trim();

        Swal.fire({
            title: "Delete " + quantity + " shares of " + symbol + "?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                table.row($(this).closest("tr")).remove().draw();
                $.ajax({
                    method: "POST",
                    url: '/deletePosition/',
                    indexValue: {
                        'symbol': symbol,
                        'quantity': quantity
                    },
                    data: {
                        'symbolId': symbolId
                    },
                    success: positionDeleteSuccess,
                    error: positionDeleteError,
                })

                function positionDeleteSuccess(data, textStatus, jqXHR) {
                    // console.log(data)
                    // console.log(textStatus)
                    // console.log(jqXHR)
                    var symbol = this.indexValue.symbol;
                    var quantity = this.indexValue.quantity;
                    Swal.fire({
                        position: 'top-end',
                        toast: true,
                        type: 'success',
                        showConfirmButton: false,
                        timer: 2800,
                        title: quantity + ' ' + symbol + ' deleted.'
                    });
                }

                function positionDeleteError(data, textStatus, jqXHR) {
                    console.log(data)
                    console.log(textStatus)
                    console.log(jqXHR)
                }
            }
        })
    });

    
    


    $("#usdPositionsTable, #cadPositionsTable").on('mousedown.edit', "i.far.fa-edit", function (e) {
        
        $(this).removeClass().addClass("far fa-save mr-2");
        var $row = $(this).closest("tr").off("mousedown");
        var $tds = $row.find("td").not(':last').not(':nth-child(7)').not(':nth-child(8)').not(':nth-child(9)');

        $.each($tds, function (i, el) {
            var txt = $(this).text().trim();
            $(this).html("").append("<input type='text' class='form-control' value=\"" + txt + "\">");
        });

    });

    $("#usdPositionsTable, #cadPositionsTable").on('mousedown', "input", function (e) {
        e.stopPropagation();
    });

    $("#usdPositionsTable, #cadPositionsTable").on('mousedown.save', "i.far.fa-save.mr-2", function (e) {

        $(this).removeClass().addClass("far fa-edit mr-2");
        var $row = $(this).closest("tr");
        var $tds = $row.find("td").not(':nth-child(8)').not(':nth-child(9)');

        $.each($tds, function (i, el) {
            var txt = $(this).find("input").val()
            $(this).html(txt);
        });
    });


    $("#usdPositionsTable, #cadPositionsTable").on('mousedown', "#selectbasic", function (e) {
        e.stopPropagation();
    });

    table = $('#usdPositionsTable, #cadPositionsTable ').DataTable({
        "paging": false,
        "searching": false,
        "ordering": false,
        "bInfo": false,
        "bAutoWidth": true,
        "fixedColumns": true,
        "bAutoWidth": true,
        "autoWidth": false
    });


    // quick add row
    // $('<div class="addRow"><button id="addRow">Add New Row</button></div>').insertAfter('#usdPositionsTable, #cadPositionsTable');
    // $('#addRow').click(function () {
    //     //t.row.add( [1,2,3] ).draw();
    //     var rowHtml = $("#newRow").find("tr")[0].outerHTML
    //     console.log(rowHtml);
    //     table.row.add($(rowHtml)).draw();
    // });

   


    
       
 }); // document.ready()

// dismiss portfolio modal
$(document).on('click', '#createPortfolioModal button[data-dismiss="modal"]', function (e) {
    e.preventDefault();
    $('#createPortfolioModal').modal().hide();
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
});

// open portfolio modal
$(document).on('click', 'a[data-target="#createPortfolioModal"]', function () {
   
    $(this).closest('.modal').hide();
    // $('#menuModal').hide();
    // $('#createPortfolioModal').modal();
});

// open Add PositionModal modal
$(document).on('click', 'button[data-target="#addPortfolioModal"]', function () {
    
    // $(this).closest('.modal').hide();
    $('#addPortfolioModal').modal();
});
