// AJAX CSRF token ---------------------------------------
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


//POSITION DELETE
// $('i.fas.fa-trash-alt').click(function(){
//     var $row = $(this).closest("tr");
//     var symbol = $row.find("td");
//     console.log('symbol');
//     console.log(symbol);
//     console.log('test');
// });


// $(this).removeClass().addClass("far fa-edit mr-2");
// var $row = $(this).closest("tr");
// console.log('$row')
// console.log($row)
// var $tds = $row.find("td").not(':nth-child(8)').not(':nth-child(9)');

// $.each($tds, function (i, el) {
//     var txt = $(this).find("input").val()
//     $(this).html(txt);
// });


// CREATE PORTFOLIO
var $myForm = $('#portfolioForm')

$myForm.submit(function (event) {
    var portfolioName = $('input#id_portfolio_name').val();
    event.preventDefault()
    var $formData = $(this).serialize()
    var $thisURL = $myForm.attr('data-url') || window.location.href // or set your own url
    $.ajax({
        method: "POST",
        url: $thisURL,
        data: $formData,
        success: handlePortfolioCreateFormSuccess,
        error: handlePortfolioCreateFormError,
    })
})

function handlePortfolioCreateFormSuccess(data, textStatus, jqXHR) {
    $('#createPortfolioModal').modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
    console.log(data)
    console.log(textStatus)
    console.log(jqXHR)
    var portfolioName = $('input#id_portfolio_name').val();
    $myForm[0].reset(); // reset form data
 
    Swal.fire({
        position: 'top-end',
        toast: true,
        type: 'success',
        showConfirmButton: false,
        timer: 2800,
        title: 'Portfolio ' + portfolioName + ' created.'
    });

}

function handlePortfolioCreateFormError(jqXHR, textStatus, errorThrown) {
    console.log(jqXHR)
    console.log(textStatus)
    console.log(errorThrown)
}

